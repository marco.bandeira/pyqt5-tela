import sys

from PyQt5.QtWidgets import (
    QMainWindow,
    QApplication,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QWidget,
    QSpacerItem,
    QSizePolicy,
    QPushButton,
    QHBoxLayout
)

class FirstForm(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle('Primeira Tela')
        self.setGeometry(20, 200, 800, 400)

        self.lbl_name = QLabel()
        self.lbl_name.setText('Nome')
        self.lbl_gender = QLabel()
        self.lbl_gender.setText('Genero')

        self.lbl_birthdate = QLabel()
        self.lbl_birthdate.setText('Data de Nascimento')
        self.lbl_salary = QLabel()
        self.lbl_salary.setText('Salário')

        self.edt_name = QLineEdit()
        self.edt_gender = QLineEdit()
        self.edt_birthdate = QLineEdit()
        self.edt_salary = QLineEdit()

        self.btn_add = QPushButton()
        self.btn_add.setText('Incluir')
        self.btn_add.clicked.connect(self._on_add)


        space = QSpacerItem(0, 0, QSizePolicy.Fixed,QSizePolicy.Expanding)

        self.H1_layout = QHBoxLayout()
        self.H1_layout.setSpacing(12)
        self.H1_layout.addWidget(self.lbl_name)
        self.H1_layout.addWidget(self.edt_name)
        self.H1_layout.addWidget(self.lbl_gender)
        self.H1_layout.addWidget(self.edt_gender)

        self.H2_layout = QHBoxLayout()
        self.H2_layout.addWidget(self.lbl_birthdate)
        self.H2_layout.addWidget(self.edt_birthdate)
        self.H2_layout.addWidget(self.lbl_salary)
        self.H2_layout.addWidget(self.edt_salary)

        self.vertical_layout = QVBoxLayout()

        self.vertical_layout.addLayout(self.H1_layout)
        self.vertical_layout.addLayout(self.H2_layout)
        self.vertical_layout.addWidget(self.btn_add)

        # self.layout.addWidget(self.btn_add)


        # self.H1_layout.addItem(space)
        # self.H2_layout.addItem(space)

        self.panel = QWidget()
        self.panel.setLayout(self.vertical_layout)
        self.setCentralWidget(self.panel)


        # self.panel2 = QWidget()

        #

        # self.setLayout(self.panel)


    def _on_add(self):

        print(f'Adicionado {self.edt_name.text()}')


app= QApplication(sys.argv)
main = FirstForm()
main.show()
sys.exit(app.exec_())
